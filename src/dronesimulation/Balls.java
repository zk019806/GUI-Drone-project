package dronesimulation;

/**
 * 
 * @author romarioalula
 *Balls are like drones and aren't allowed to get close to things 
 */
public class Balls extends Drone {

    private static int start = 0;

    /**
     * instantiate balls
     * @param X int
     * @param Y int
     */
    public Balls(double X, double Y) {
        super(X, Y);
        //unique ID
        ID = start++;
        speed = 4;
        size = 6;
        colour = 'g';

    }

    @Override
    /**
     * print location of the instantiated balls
     */
    public String toString() {
        return "Balls " + ID + " is at " + String.format("%.2f",xPosition) + ", " + String.format("%.2f",yPosition) + " moving at angle " + String.format("%.2f",angle);
    }

    @Override
    /**
     * confirm or reject balls is at that location, and also extra space for drones that can't go near balls
     */
    public boolean isHere(double x, double y) {
        //include size of piece + extra radius
        double top = y+size+size*2;
        double bottom = y-size-size*2;
        double left = x-size-size*2;
        double right = x+size+size*2;
        //check if there
        if((xPosition <= right && xPosition >= left) && (yPosition <= top && yPosition >= bottom)) {
            return true;
        }

        return false;
    }
}
